import os
from configparser import ConfigParser

BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

config = ConfigParser()
config.read(BASE_DIR + "/settings/config.ini")

URL_OBJECT = "postgresql+psycopg://{}:{}@{}:{}/{}".format(
    config["DB"]["PSQL_USER"],
    config["DB"]["PSQL_PASSWORD"],
    config["DB"]["PSQL_HOST"],
    config["DB"]["PSQL_PORT"],
    config["DB"]["PSQL_DB_NAME"],
)
