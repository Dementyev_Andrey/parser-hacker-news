import os, sys, requests
from bs4 import BeautifulSoup
from sqlalchemy import create_engine, text

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from settings.general_settings import URL_OBJECT

class Article:
    def __init__(self, id, title, author, date, link):
        self.id: int = id
        self.title: str = title
        self.author: str = author
        self.date: str = date
        self.link: str = link

    def __repr__(self) -> str:
        return f"{self.title}"

def get_article(lst) -> Article | None:
    obj = {}
    for element in lst:
        atribute_class = element.attrs.get("class", [None])
        match atribute_class[0]:
            case "athing":
                artikle_link = element.find(class_="titleline").find("a")
                obj["id"] = int(element["id"])
                obj["title"] = artikle_link.text
                obj["link"] = artikle_link.attrs["href"]
            case None:
                obj["author"] = element.find(class_="hnuser").text\
                              if element.find(class_="hnuser")\
                              else "-"
                obj["date"] = element.find(class_="age").text\
                            if element.find(class_="age") \
                            else "-"
    return (
        Article(
            id=obj["id"],
            title=obj["title"],
            author=obj["author"],
            date=obj["date"],
            link=obj["link"],
        )
        if obj.get("id")
        else None
    )

def chunks(lst, size) -> list:  # type: ignore
    for i in range(0, len(lst), size):
        yield lst[i : i + size]
        
        
if __name__ == "__main__":
    r = requests.get("https://news.ycombinator.com/news", timeout=60)
    soup = BeautifulSoup(r.content)

    element = soup.find(class_="athing")
    table = element.find_parent().select("tr")
    for chunck in chunks(table, 3):
        article = get_article(chunck)
        if article:
            print("🐍 File: parser-hacker-news/main_parcer.py | Line: 60 | undefined ~ article",article)
            print("🐍 File: parser-hacker-news/main_parcer.py | Line: 61 | undefined ~ link",article.link)
        
    engine = create_engine(URL_OBJECT, echo=True)
    with engine.connect() as connection:
        connection.execute(text("select version()"))